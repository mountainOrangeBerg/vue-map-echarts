import axios from 'axios';
import qs from 'qs'
// import da from "element-ui/src/locale/lang/da";
// import manage from './manage.js'
// axios.defaults.baseURL = manage.apiHost;
// import { getToken,clear } from '@/utils/auth'
axios.defaults.timeout = 60 * 1000 * 10;
// axios.defaults.cache = false

// console.log("当前环境",process.env.NODE_ENV);

// /*第一层if判断生产环境和开发环境*/
// if (process.env.NODE_ENV === 'production') {
//     /*第二层if，根据.env文件中的VUE_APP_FLAG判断是生产环境还是测试环境*/
//     if (process.env.VUE_APP_FLAG === 'pro') {
//         //production 生产环境
//         axios.defaults.baseURL = 'http://127.0.0.1:8080';
//     } else {
//         //test 测试环境
//         axios.defaults.baseURL = 'http://127.0.0.1:8080';
//     }
// } else {
//     //dev 开发环境
//     // axios.defaults.baseURL = 'http://127.0.0.1:8080';
// }
//
// let baseUrl = axios.defaults.baseURL;
//
// console.log("当前host：", baseUrl);

axios.interceptors.request.use((request) => {
    /**
     * 全局拦截请求发送前提交的参数
     * 以下代码为示例，在登录状态下，分别对 post 和 get 请求加上 token 参数
     * 不拦截登陆获取token接口
     */
    request.headers = {
            'Content-Type': 'application/json',
            'Shopid': 86331630,
            'Userid': '9999'
        }
    if(request.url == "https://erp2.youhaosoft.com/YHWeb/BILogin") return request

    request.headers.token = localStorage.token

    request.headers.token =getToken()


    return request
})
axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        return Promise.reject(error);
    }
);
//获取token
function getToken(){
    // 发送POST请求
    axios.post('https://erp2.youhaosoft.com/YHWeb/BILogin', {
        "shopId": 86331630,
        "userId": '9999',
        "params":{
            "userPwd":'sskp9999'
        }
    }).then(response => {
            // console.log(response.data.data.token);

                localStorage.token = response.data.data.token;

        })
        .catch(error => {
            // 请求失败的处理逻辑
            console.error('Error:', error);
        });
    return localStorage.token;
};

function checkStatus(response) {
    if (response.status === 200 || response.status === 304) {
        return response['data'];
    } else {
        throw new Error(response.data.message); // eslint-disable-line
    }
    throw new Error(response.statusText); // eslint-disable-line
}

export function request(config) {
    return axios.request(config).then(checkStatus);
}

export function get(url, params) {
    // let queryData = Object.assign(params, {t: new Date().getTime()})
    return axios.get(url, {params}).then(checkStatus);
}

export function delet(url, params) {
    return axios.delete(url, params).then(checkStatus);
}

export function post(url, data, config) {
    return axios.post(url, qs.stringify(data), config).then(checkStatus);
}

export function postDefine(url, data, config) {
    return axios.post(url, data, config).then(checkStatus);
}

export function postData(url, data, config) {
    return axios.post(url, data, config).then(checkStatus);
}

export function put(url, data, config) {
    return axios.put(url, data, config).then(checkStatus);
}

export function patch(url, data, config) {
    return axios.patch(url, data, config).then(checkStatus);
}
